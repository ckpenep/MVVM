package android.test.mvvmproject.di.modules

import android.content.Context
import android.test.mvvmproject.data.repository.login.LoginRepositoryImpl
import android.test.mvvmproject.domain.repository.LoginRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [AppModule::class, RetrofitApiModule::class])
class RepositoryModule {

    @Provides
    @Singleton
    fun provideLoginApi(context: Context, retrofit: Retrofit): LoginRepository =
        LoginRepositoryImpl(context, retrofit)
}