package android.test.mvvmproject.mvvm

import android.app.Application
import android.test.mvvmproject.MVVMApp
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseViewModel(val app: Application): ViewModel() {

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    init {
        MVVMApp[app].appComponent.inject(this)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}