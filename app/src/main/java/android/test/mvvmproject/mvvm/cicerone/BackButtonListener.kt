package android.test.mvvmproject.mvvm.cicerone

interface BackButtonListener {
    fun onBackPressed(): Boolean
}