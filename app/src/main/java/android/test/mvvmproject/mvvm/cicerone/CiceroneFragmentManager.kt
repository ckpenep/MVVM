package android.test.mvvmproject.mvvm.cicerone

import android.test.mvvmproject.mvvm.BaseViewModel
import android.test.mvvmproject.presentation.fragments.BaseFragment

interface CiceroneFragmentManager {
    fun <P: BaseViewModel> replaceFragment(fragment: BaseFragment<P>)

    fun <P: BaseViewModel> addFragment(fragment: BaseFragment<P>)

    fun <P: BaseViewModel> newRootFragment(fragment: BaseFragment<P>)

    fun exit()
}