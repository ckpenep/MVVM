package android.test.mvvmproject

import android.app.Activity
import android.app.Application
import android.content.Context
import android.test.mvvmproject.di.components.AppComponent
import android.test.mvvmproject.di.components.DaggerAppComponent
import android.test.mvvmproject.di.modules.AppModule
import android.test.mvvmproject.di.modules.RetrofitApiModule

class MVVMApp: Application() {

    companion object{
        operator fun get(activity: Activity): MVVMApp {
            return activity.application as MVVMApp
        }

        operator fun get(context: Context): MVVMApp {
            return context as MVVMApp
        }
    }

    override fun onCreate() {
        super.onCreate()
    }

    val appComponent: AppComponent by lazy {
        initDagger()
    }

    private fun initDagger(): AppComponent =
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .retrofitApiModule(RetrofitApiModule())
            .build()
}