package android.test.mvvmproject.presentation.activities

import android.content.Context
import android.os.Bundle
import android.test.mvvmproject.MVVMApp
import android.test.mvvmproject.R
import android.test.mvvmproject.di.components.ActivityComponent
import android.test.mvvmproject.di.modules.ActivityModule
import android.test.mvvmproject.mvvm.BaseViewModel
import android.test.mvvmproject.mvvm.cicerone.BackButtonListener
import android.test.mvvmproject.mvvm.cicerone.CiceroneFragmentManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import java.util.concurrent.TimeUnit
import javax.inject.Inject

abstract class BaseActivity <V : BaseViewModel>: AppCompatActivity() {

    protected lateinit var viewModel: V

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var activityComponent: ActivityComponent

    @Inject
    lateinit var requests: CompositeDisposable

    @Inject
    lateinit var cicerone: Cicerone<Router>

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var ciceroneNavigator: SupportAppNavigator

    @Inject
    lateinit var fragmentManager: CiceroneFragmentManager

    @LayoutRes
    abstract fun layout(): Int
    abstract fun initialization()
    abstract fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initFragmentComponent()

        viewModel = provideViewModel(viewModelFactory)

        if (layout() != 0) {
            setContentView(layout())
            initialization()
        }
    }

    fun initFragmentComponent() {
        if (!::activityComponent.isInitialized) {
            activityComponent = MVVMApp[this].appComponent.plus(ActivityModule(R.id.container, this as BaseActivity<BaseViewModel>))
            activityComponent.inject(this)
        }
    }

    override fun onDestroy() {
        requests.clear()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(ciceroneNavigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager
        var fragment: Fragment? = null
        val fragments = fm.fragments
        for (f in fragments) {
            if (f.isVisible) {
                fragment = f
                break
            }
        }

        if (fragment != null
            && fragment is BackButtonListener
            && (fragment as BackButtonListener).onBackPressed()) {
        } else {
            fragmentManager.exit()
        }
    }

    fun toggleKeyboard(show: Boolean) {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (!show)
            inputMethodManager.hideSoftInputFromWindow(window.decorView.windowToken, 0)
        else
            inputMethodManager.toggleSoftInputFromWindow(window.decorView.windowToken, InputMethodManager.SHOW_FORCED, 0)
    }


    fun View.getClick(durationMillis: Long = 500, onClick:(view: View)->Unit){
        RxView.clicks(this)
            .throttleFirst(durationMillis, TimeUnit.MILLISECONDS)
            .subscribe ({ _ ->
                onClick(this)
            }, {

            })
            .also { requests.add(it) }
    }
}