package android.test.mvvmproject.presentation.activities.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.test.mvvmproject.R
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
