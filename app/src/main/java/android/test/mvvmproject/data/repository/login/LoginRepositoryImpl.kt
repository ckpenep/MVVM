package android.test.mvvmproject.data.repository.login

import android.content.Context
import android.test.mvvmproject.MVVMApp
import android.test.mvvmproject.domain.repository.LoginRepository
import retrofit2.Retrofit
import javax.inject.Inject

class LoginRepositoryImpl @Inject constructor(val context: Context, val retrofit: Retrofit): LoginRepository {

    @Inject
    lateinit var loginApiInterface: LoginApiInterface

    init {
        MVVMApp[context].appComponent.inject(this)
    }


}